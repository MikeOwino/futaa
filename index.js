const express = require("express");
const request = require("request");
const cheerio = require("cheerio");

const app = express();

// Fetch the HTML content
request(
  "https://thivinanandh.github.io/Football_Streaming_Links/index.html",
  (error, response, body) => {
    if (!error && response.statusCode == 200) {
      // Load the HTML content into Cheerio for parsing and modification
      const $ = cheerio.load(body);

      // Add the <link> tag to the <head> section
      $("head").append(
        '<link rel="shortcut icon" href="https://nextjs.org/static/favicon/apple-touch-icon.png" type="image/x-icon">'
      );
      $(
        'head link[href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"]'
      ).attr(
        "href",
        "https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"
      );

      // Update the <title> tag to "Futaa"
      $("title").text("Futaa");

      // Find all elements with class="list-group-item list-group-item-action" and add target="_blank" attribute
      $(".list-group-item.list-group-item-action")
        .attr("target", "_blank")
        .css("color", "#007bff");

      $(".list-group-item.list-group-item-action").each(function () {
        var link = $(this);
        link.html(
          '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-external-link"><path d="M18 13v6a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h6"></path><polyline points="15 3 21 3 21 9"></polyline><line x1="10" y1="14" x2="21" y2="3"></line></svg><svg height="21" viewBox="0 0 21 21" width="21" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd" transform="translate(3 5)"><path d="m2.49278838.53409401 10.00000002-.03605833c1.1045623-.00398287 2.0032157.88821306 2.0071986 1.99277538.0000087.00240386.000013.00480774.000013.00721162v5.00197732c0 1.1045695-.8954305 2-2 2h-10c-1.1045695 0-2-.8954305-2-2v-4.965919c0-1.10175423.89104131-1.99601428 1.99278838-1.99998699z" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/><path d="m9.46666667 4.6-2.66666667-2c-.2209139-.16568542-.53431458-.1209139-.7.1-.06491106.08654809-.1.19181489-.1.3v4c0 .27614237.22385763.5.5.5.10818511 0 .21345191-.03508894.3-.1l2.66666667-2c.2209139-.16568542.26568542-.4790861.1-.7-.02842713-.03790283-.06209717-.07157288-.1-.1z" fill="currentColor" fill-rule="nonzero"/><path d="m2.464 11.5h10.036" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/></g></svg>' +
            link.html()
        );
      });

      // replace <nav> with new nav element
      const newNav = `
  <nav style="text-align: center; padding-top: 10px; padding-bottom: 10px">
    <a href="/"><img src="https://res.cloudinary.com/weknow-creators/image/upload/v1676829976/goal_1_odjrjo.gif" height="100px" alt="logo"></a>
  </nav>
`;
      $("nav").replaceWith(newNav);

      // Find the </nav> tag and insert an <h2> tag with the text "Mike" after it
      $("nav").after(
        '<h4 style="text-align: center;">For streams without ads install <a href="https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm?hl=en-US" target="_blank">uBlock Origin</a></h4>'
      );

      // Modify the padding-top value of the .container class to 30px
      $(".container").css("padding-top", "30px");

      // Find the first <p> element and replace it with a new <p> element with the specified attributes
      const newParagraph =
        '<h6 style="text-align: center;"><strong>All links are pulled from <a href="http://www.redditsoccerstreams.tv" target="_blank">redditsoccerstreams</a>. This site not host or upload any video & media files, we are not responsible for the legality of the content of other linked or embedded sites. If you have any legal issues please contact appropriate media owners or hosters</strong></h6>';
      $("p:first-of-type").replaceWith(newParagraph);

      // Set the modified HTML content to a variable for later use
      app.locals.htmlContent = $.html();
    } else {
      console.error(`Error fetching HTML content: ${error}`);
    }
  }
);

// Serve the HTML content on a GET request
// app.get("/", (req, res) => {
//   res.send(app.locals.htmlContent || "<h2>Waiting for Server updates...</h2>");
// });

app.get("/", (req, res) => {
  res.send(app.locals.htmlContent);
});

// Start the server
app.listen(3000, () => {
  console.log("Server started on port 3000");
});
