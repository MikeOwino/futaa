const express = require("express");
const request = require("request");
const cron = require("node-cron");
const cheerio = require("cheerio");

const app = express();

// Schedule a cron job to fetch the HTML content after every 10 minutes
cron.schedule("*/10 * * * *", () => {
  request(
    "https://cdn.jsdelivr.net/gh/thivinanandh/Football_Streaming_Links@master/index.html",
    (error, response, body) => {
      if (!error && response.statusCode == 200) {
        // Load the HTML content into Cheerio for parsing and modification
        const $ = cheerio.load(body);

        // Add the <link> tag to the <head> section
        $("head").append(
          '<link rel="shortcut icon" href="https://nextjs.org/static/favicon/apple-touch-icon.png" type="image/x-icon">'
        );
        // Update the <title> tag to "Futaa"
        $("title").text("Futaa");

        // Find all elements with class="list-group-item list-group-item-action" and add target="_blank" attribute
        $(".list-group-item.list-group-item-action").attr("target", "_blank");
        // Find the </nav> tag and insert an <h2> tag with the text "Mike" after it
        $("nav").after(
          '<h3 style="text-align: center;">For streams without ads install <a href="https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm?hl=en-US" target="_blank">uBlock Origin</a></h3>'
        );

        // Modify the padding-top value of the .container class to 30px
        $(".container").css("padding-top", "30px");

        // Find the first <p> element and replace it with a new <p> element with the specified attributes
        const newParagraph =
          '<p style="text-align: center;"><strong>All links are pulled from <a href="http://www.redditsoccerstreams.tv" target="_blank">redditsoccerstreams</a>. This site not host or upload any video & media files, we are not responsible for the legality of the content of other linked or embedded sites. If you have any legal issues please contact appropriate media owners or hosters</strong></p>';
        $("p:first-of-type").replaceWith(newParagraph);

        // show success message of cron run
        console.log("HTML content fetched successfully");

        // Set the modified HTML content to a variable for later use
        app.locals.htmlContent = $.html();
      } else {
        console.error(`Error fetching HTML content: ${error}`);
      }
    }
  );
});

// Serve the HTML content on a GET request
app.get("/", (req, res) => {
  res.send(app.locals.htmlContent || "<h2>No content available yet</h2>");
});

// Start the server
app.listen(3000, () => {
  console.log("Server started on port 3000");
});
